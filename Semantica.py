from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords
from nltk import StanfordPOSTagger
from nltk.stem import PorterStemmer
from nltk.wsd import lesk
import numpy
import string

table = {ord(char): None for char in string.punctuation}
stop_words = set(stopwords.words('english')) 

file_test = open("test.txt", "r", encoding="utf-8")
phrase_processed = []

phrase = file_test.readline()
target_word = file_test.readline().strip()
window_size = int(file_test.readline())

phrase = phrase.lower() # to lower
phrase = phrase.translate(table) # eliminate punctuation
phrase = phrase.split()

myTagger = StanfordPOSTagger("english-bidirectional-distsim.tagger", "stanford-postagger.jar")
pos_tagging_output = myTagger.tag(phrase) # get part of speech

porter = PorterStemmer()
target = None       # pair of (word, part of speech)
words_with_parts_of_speech = []

for pair in pos_tagging_output:
    if pair[0] not in stop_words:                           # eliminate stop words
        word = porter.stem(pair[0])                      # stemming
        words_with_parts_of_speech.append((word, pair[1]))
            
found_target_at_idx = 0
for pair in words_with_parts_of_speech:
    if pair[0] == target_word:
        target = pair
        break
        
    found_target_at_idx += 1
    

neighbors = []          # list of pairs (word, part of speech) neighboring the target word

left_window = window_size
right_window = window_size

if found_target_at_idx - window_size < 0:
    left_window = window_size + (found_target_at_idx - window_size)
    right_window = window_size + (window_size - left_window)
    
if found_target_at_idx + 1 + window_size >= len(words_with_parts_of_speech):
    right_window = window_size - (found_target_at_idx + 1 + window_size - len(words_with_parts_of_speech))
    left_window = window_size + (window_size - right_window)

for i in range(found_target_at_idx - left_window, found_target_at_idx):
    neighbors.append(words_with_parts_of_speech[i])

for i in range(found_target_at_idx + 1, found_target_at_idx + 1 + right_window):
    neighbors.append(words_with_parts_of_speech[i])

print("target word = " + str(target))
print("neighbors = " + str(neighbors))


def compute_gloss(synset):
    gloss__word = []
    gloss__word.append(synset.definition())
    syn_set_hypernyms = synset.hypernyms()
    gloss = " "
    for synset_h in syn_set_hypernyms:
        gloss += synset_h.definition()
    gloss__word.append(gloss)
    syn_set_hyponyms = synset.hyponyms()
    gloss = " "
    for synset_h in syn_set_hyponyms:
        gloss += synset_h.definition()
    gloss__word.append(gloss)
    syn_set_meronyms = synset.part_meronyms()
    gloss = " "
    for synset_m in syn_set_meronyms:
        gloss += synset_m.definition()
    gloss__word.append(gloss)
    syn_set_holonyms = synset.part_holonyms()
    gloss = " "
    for synset_h in syn_set_holonyms:
        gloss += synset_h.definition()
    gloss__word.append(gloss)
    return gloss__word


def scor_gloss(gloss_one, gloss_two):
    n = len(gloss_one)
    m = len(gloss_two)
    dp = numpy.zeros((n + 1, m + 1))
    result = -1
    start_idx_gloss_one = -1
    end_idx_gloss_one = -1
    start_idx_gloss_two = -1
    end_idx_gloss_two = -1

    for i in range(0, n + 1):
        for j in range(0, m + 1):
            if i == 0 or j == 0:
                dp[i][j] = 0
            elif gloss_one[i - 1] == gloss_two[j - 1]:
                dp[i][j] = dp[i - 1][j - 1] + 1
                if dp[i][j] > result:
                    result = dp[i][j]
                    start_idx_gloss_one = i - result
                    end_idx_gloss_one = i
                    start_idx_gloss_two = j - result
                    end_idx_gloss_two = j
            else:
                dp[i][j] = 0

    if result != -1:
        new_gloss_one = gloss_one[0:int(start_idx_gloss_one)] + gloss_one[int(end_idx_gloss_one):n]
        new_gloss_two = gloss_two[0:int(start_idx_gloss_two)] + gloss_two[int(end_idx_gloss_two):m]

        real_score = 0
        sub_sequence = gloss_one[int(start_idx_gloss_one):int(end_idx_gloss_one)]
        for word in sub_sequence:
            if word not in stop_words:
                real_score += 1

        return real_score**2 + scor_gloss(new_gloss_one, new_gloss_two)
    else:
        return 0
            
            
def compute_relatedness(synset_A, synset_B):
    scor = 0
    gloss_A = compute_gloss(synset_A)
    gloss_B = compute_gloss(synset_B)
    for gloss_one in gloss_A:
        gloss_one = gloss_one.lower()
        gloss_one = gloss_one.split()
        for gloss_two in gloss_B:
            gloss_two = gloss_two.lower()
            gloss_two = gloss_two.split()
            scor = scor + scor_gloss(gloss_one, gloss_two) + scor_gloss(gloss_two, gloss_one)

    return scor
    
                    
def compute_sense_scor(synset_target_word, neighbors):
    score = 0
    for neighbor_word in neighbors:

        pos = ''
        if neighbor_word[1].lower()[0] == 'n':
            pos = 'n'
        elif neighbor_word[1].lower()[0] == 'v':
            pos = 'v'
        elif neighbor_word[1].lower()[0] == 'j':
            pos = 'a'
        elif neighbor_word[1].lower()[0] == 'r':
            pos = 'r'

        if pos != '':
            syn_sets_n = wn.synsets(neighbor_word[0], pos=pos)
        else:
            syn_sets_n = wn.synsets(neighbor_word[0])
        for synset_n in syn_sets_n:
            score += compute_relatedness(synset_target_word, synset_n)
    return score


def return_sense(target_word, neighbors):
    syn_sets = wn.synsets(target_word, pos="n")
    max_score = 0
    for synset in syn_sets:
        score = compute_sense_scor(synset, neighbors)
        if score > max_score:
            max_score = score
            best_synset = synset
            
    # daca doua scrouri egale cel mar relevant din wordnet
    return best_synset


# synset = return_sense(target_word, neighbors)
# print(synset.definition())
# print(synset.name())
# print(synset.lexname())
#print([w.lexname() for w in wn.synsets('line')])
print(lesk("w7_006:6674: <s> Mr. Fowler's action appears aimed at quelling growing congressional opposition to any increase. </s> @ <s> The subscriber-line charge, also called the access charge, makes subscribers pay part of the fixed costs of the lines to their homes and small businesses rather than adding these costs to the cost of long-distance service. </s>", target_word, pos="n"))


#compute accuracy with our implementation and with lesk